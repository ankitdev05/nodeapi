const startupDebugger = require('debug')('app:starup');
const dbDebugger = require('debug')('app:db');
const logger = require('./logger');
const config = require('config');
const courses = require('./routes/courses');
const express = require("express");
const app = express();
const helmet = require('helmet');
const morgan = require('morgan');

app.set('view engine', 'pug');
app.set('views', './views');

console.log(`Node ENV : ${process.env.NODE_ENV}`);
console.log(`app: ${app.get('env')}`);

console.log('application name: '+ config.get('name'));
console.log('mail server: '+ config.get('mail.host'));
console.log('mail password: '+ config.get('mail.password'));

app.use(express.json());
app.use(express.urlencoded({ extended:true })); //key=value in url encoded   
app.use(express.static('public'));
app.use(helmet());
app.use('/courses', courses);


if(app.get('env') === 'development') {
    app.use(morgan('tiny'));
    startupDebugger("Morgan enabled..");
}


// Db debugger
dbDebugger('Connect to the ddatabase');
app.use(logger);

router.get('/', (req, res) => {
    //res.send('Hello world');
    res.render('index', {title:'My express app', message:'Hello'});
});

const port = process.env.PORT || 3000;
app.listen(port, ()=> console.log(`Listening on port ${port}`));